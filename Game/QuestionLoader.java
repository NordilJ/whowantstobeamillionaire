import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuestionLoader {
    public static List<QuestionGroup> loadQuestions() {
        List<QuestionGroup> questionGroups = new ArrayList<>();

        questionGroups.add(
                new QuestionGroup(
                        50,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("What NBA stands for?", "National Basketball Association", "New Bus Association", "National Baseball Association", "Never been accessed", "A"),
                                new Question("Who plays in the first Space Jam movie?", "Tihomir Angelov", "Dinko Todorov", "Michael Jordan", "Kobe Bryant", "C"),
                                new Question("Which team represent Chicago in the NBA?", "Hornets", "Bulls", "Lakers", "Wizards", "B"),
                                new Question("Where did Kobe Bryant played trough his whole career?", "Merilend", "Utah Jazz", "Charlotte Hornets", "Los Angeles Lakers", "D"),
                                new Question("What is double dribble?", "Illegal dribble", "Winning the game", "Fouling someone", "No such thing in NBA", "A")))));


        questionGroups.add(
                new QuestionGroup(
                        100,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("With which number Michael Jordan played in Bulls?", "11", "23", "8", "17", "B"),
                                new Question("Which team represent Orlando?", "Pelicans", "Kicks", "Magic", "Clippers", "C"),
                                new Question("Which of those players never played in NBA?", "Larry Bird", "Hristo Stoichkov", "Luka Doncic", "Zach LaVine", "B"),
                                new Question("Which is not part of the All star weekend?", "Skill challenge", "Three point contest", "Dunk Contest", "Sumo wrestling", "D"),
                                new Question("Where did Lebron James played college basketball?", "He went to NBA from high school", "Duke", "North Carolina", "UCLA", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        150,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("What did Michael Jordan played in the 1994?", "Baseball", "Basketball", "NFL", "Became coach", "A"),
                                new Question("What dose MVP stands for?", "Most handsome player", "Most vaccinated player", "Most valuable player", "Mid variation player", "C"),
                                new Question("Finnish the following sentence to represent one of the greatest ball players nickname Air .....", "Bird", "Jordan", "Ball", "Conditioner", "B"),
                                new Question("What is NBA draft?", "Technical foul", "Game analysis", "Type of interview", "Recruiting college/high school players", "D"),
                                new Question("Greg Popovich is the coach of which team?", "San Antonio Spurs", "Duke", "Minnesota Timberwolves", "Levski Sofia", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        200,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("Which player had the nickname Black Mamba?", "Kobe Bryant", "Zach LaVine", "Kristap Porzingis", "Rasheed Wallace", "A"),
                                new Question("At what position did Shaquille O'Neil played?", "Comedian", "Guard", "Center", "At the BBQ", "C"),
                                new Question("Which duo never won NBA title?", "Kobe and Shaq", "Malone and Stockton", "Jordan and Pippen", "Curry and Klay", "B"),
                                new Question("Which of the following teams won the title 3 consecutive years?", "Portland Trailblazers", "New York Nicks", "Houston Rockets", "Los Angeles Lakers", "D"),
                                new Question("Which of the players never won the MVP title?", "Brian Scalabrine", "Steph Curry", "Kevin Durant", "Michael Jordan", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        250,
                        true,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("Which of the following teams has not won an NBA championship?", "LA Clippers", "Los Angeles Lakers", "New York Knicks", "Miami Heat", "A"),
                                new Question("Who were the big three in Boston and won title in 2008?", "Bird, McHale, Parish", "Kobe, Shaq, Gasol", "Garnet, Allen, Pierce", "Durant, Curry, Klay", "C"),
                                new Question("With which team did NBA legend Larry Bird play his entire professional career?", "Miami Heat", "Boston Celtics", "Detroit Pistons", "New York Knicks", "B"),
                                new Question("What was Allen Iverson nickname?", "The StepOver", "The Crossover", "The GOAT", "The Answer", "D"),
                                new Question("Which of the following players never won championship title?", "Charles Barkley", "Kobe Bryant", "Bill Russell", "Ben Wallace", "A")))));


        questionGroups.add(
                new QuestionGroup(
                        500,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("Which player never won a dunk contest?", "Lebron James", "Kobe Bryant", "Vince Carter", "Zach LaVine", "A"),
                                new Question("Which player never won a three point contest?", "Craig Hodges", "Steve Kerr", "Reggie Miller", "Peja Stoyakovic", "C"),
                                new Question("Which player never won rookie of the year?", "Pau Gasol", "Magic Johnson", "Michael Jordan", "Larry Bird", "B"),
                                new Question("Which player won rookie of the year?", "Bill Russel", "Magic Johnson", "Kobe Bryant", "Larry Bird", "D"),
                                new Question("Which player is name the beard?", "James Harden", "Tyson Chandler", "Nikola Pekovic", "Carlos Boozer", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        1000,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("How many rings dose Charles Barkley have?", "0", "3", "1", "2", "A"),
                                new Question("From which country is Luka Doncic?", "Lithuania", "Slovakia", "Slovenia", "Estonia", "C"),
                                new Question("Which player holds the single-game scoring record?", "Bill Russel", "Wilt Chamberlain", "Devin Booker", "Kobe Bryant", "B"),
                                new Question("From which country is Tony Parker?", "Canada", "Spain", "France", "Argentina", "C"),
                                new Question("From which country is Manu Ginobili?", "Argentina", "Spain", "France", "Canada", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        1500,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("Which of the following players played with Kobe Bryant?", "Denis Rodman", "Michael Jordan", "Scottie Pippen", "Jimmy Butler", "A"),
                                new Question("Which of the following players is cousin of Tracy McGrady?", "Rasheed Wallace", "Ben Wallace", "Vince Carter", "Yao Ming", "C"),
                                new Question("What is the nickname of Kawhi Leonard?", "The Man", "The Claw", "The Cyborg", "The Joker", "B"),
                                new Question("Which of the following is not from the ball brothers?", "Lonzo", "LaMello", "LiAngelo", "Lavar", "D"),
                                new Question("Who played Sidney Deane in the movie - White can't jump?", "Wesley Snipes", "Woody Harrelson", "Denzel Washignton", "Omar Epps", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        2000,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("Which of the following teams has not won an NBA championship?", "Orlando Magic", "Portland Trailblazers", "Milwaukee Bucks", "Houston Rockets", "A"),
                                new Question("The Golden State Warriors are based in which city?", "Los Angeles", "Las Vegas", "San Francisco", "San Jose", "C"),
                                new Question("In what year was the 3 point field goal introduced to the NBA?", "1988", "1979", "1965", "1947", "B"),
                                new Question("Who is on the logo of the NBA?", "Michael Jordan", "Julias Erving", "Bill Russell", "Jerry West", "D"),
                                new Question("What is Magic Johnson’s real first name?", "Ervin", "Earl", "Eaton", "Ben", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        2500,
                        true,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("Which player holds the record for the most career playoff points in the NBA?", "Lebron James", "Bill Russell", "Michael Jordan", "Wilt Chamberlain", "A"),
                                new Question("Who is the only player to win the NBA Finals MVP Award while being on the losing team?", "Wilt Chamberlain", "Michael Jordan", "Jerry West", "Bill Russell", "C"),
                                new Question("Which team holds the record for the longest championship streak?", "Detroit Pistons", "Boston Celtics", "Los Angeles Lakes", "Chicago Bulls", "B"),
                                new Question("Which player starred as the co-pilot Roger Murdock in the 1980 movie “Airplane!”?", "Larry Bird", "Magic Johnson", "Julius Erving", "Kareem Abdul-Jabbar", "D"),
                                new Question("Which team won the first NBA championship in 1947?", "The Philadelphia Warriors", "New York Knicks", "St. Louis Bombers", "Baltimore Bullets", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        5000,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("How many NBA teams were created this millennium?", "3", "4", "2", "1", "A"),
                                new Question("What year was the NBA founded?", "1943", "1954", "1946", "1951", "C"),
                                new Question("Which NBA legend had the nickname, “The Round Mound of Rebound”?", "Walter Dukes", "Charles Barkley", "Bill Russell", "Wilt Chamberlain", "B"),
                                new Question("Which player has won the MVP award a record 6 times?", "Wilt Chamberlain", "Bill Russell", "Michael Jordan", "Kareem Abdul-Jabbar", "D"),
                                new Question("Who was the first non-American player to win the NBA Rookie of the Year award?", "Pau Gasol", "Peja Stoyakovic", "Luka Doncic", "Emeka Okafor", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        10000,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("How many three-pointers has Shaq made?", "1", "2", "0", "7", "A"),
                                new Question("Which of these musical artists has recorded a song with Shaquille O'Neal?", "The Notorious B.I.G", "Michael Jackson", "All of them", "Jay-Z", "C"),
                                new Question("Who holds the record for most free-throws made in a row?", "Steeve Nash", "Michael Williams", "Chris Paul", "Steph Curry", "B"),
                                new Question("Which of these teams was never part of the NBA?", "Washington Bullets", "Detroit Falcons", "Vancouver Grizzlies", "Dallas Rockets", "D"),
                                new Question("Who was the first NBA player to break a backboard?", "Darryl Dawkins", "Kareem Abdul-Jabbar", "Wilt Chamberlain", "Shaq", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        25000,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("Who is the only woman to ever be officially drafted by an NBA team?", "Lusia Harris", "Lisa Leslie", "Elena Delle Donne", "Sheryl Swoopes", "A"),
                                new Question("Who was the first non-white player in the NBA?", "Earl Francis Lloyd", "Vern Mikkelsen", "Wat Misaka", "Nathaniel Clifton", "C"),
                                new Question("Which of the following players scored the first quadruple-doubles?", "Phil Jackson", "Nate Thurmond", "Jerry West", "Willis Reed", "B"),
                                new Question("The first time the NBA finals went to 7 games was in 1950-51. Which team won?", "Minneapolis Lakers", "New York Knicks", "Boston Celtics", "Rochester Royals", "D"),
                                new Question("The winner of which award receives the Maurice Podoloff Trophy?", "MVP", "ROY", "Finals MVP", "Player who reach 20 000 points", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        50000,
                        false,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("Which two teams contested what the NBA considers the first game in NBA history?", "Toronto Huskies and New York Knickerbockers", "Washington Capitols and New York Knicks", "Minneapolis Lakers and Rochester Royals", "Indianapolis Olympians and Anderson Packers", "A"),
                                new Question("Which player hold the record for most consecutive ejections?", "Bill Laimbeer", "Dick Hemric", "Don Boven", "Isiah Thompson", "C"),
                                new Question("Who hold the record for most games played in a single season?", "Theo Ratliff", "Walt Bellamy", "Michael Jordan", "Shareef Abdur-Rahim", "B"),
                                new Question("Which player hold the record for most blocks in a single game?", "Mark Eaton", "Manute Bol", "Shaquille O'Neal", "Elmore Smith", "D"),
                                new Question("Which player has most steals during a single quarter?", "Fat Lever", "Lou Williams", "Draymond Green", "Brandon Roy", "A")))));

        questionGroups.add(
                new QuestionGroup(
                        100000,
                        true,
                        new ArrayList<Question>(Arrays.asList(
                                new Question("Which player holds the record for most consecutive free-throws made?", "Tom Amberry", "Steve Nash", "Dirk Nowitzki", "Jose Calderon", "A"),
                                new Question("Which team have the record of most fouls during a single game?", "Detroit Pistons", "New York Knicks", "Utah Jazz", "Boston Celtics", "C"),
                                new Question("Who was the fourth winner of the Most Improved Player of the Year award in NBA history?", "Alvin Robertson", "Kevin Johnson", "Scott Skiles", "Dale Ellis", "B"),
                                new Question("Which player has the most blocked shots in one season?", "Manute Bol", "Patrick Ewing", "Dikembe Mutombo", "Mark Eaton", "D"),
                                new Question("Who holds the record for most missed shots in the NBA?", "Kobe Bryant", "John Havlicek", "LeBron James", "Elvin Hayes", "A")))));



        return questionGroups;
    }
}
