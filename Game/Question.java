import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Question {
    private String questionText;
    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;
    private String answer;

    private boolean isFiftyFiftyUsed;

    public Question(String questionText, String optionA, String optionB, String optionC, String optionD, String answer) {
        this.questionText = questionText;
        this.optionA = optionA;
        this.optionB = optionB;
        this.optionC = optionC;
        this.optionD = optionD;
        this.answer = answer;
        this.isFiftyFiftyUsed = false;
    }

    public void print(){
        System.out.println(this.questionText);
        if (this.isFiftyFiftyUsed){
            switch (this.answer){
                case "A":
                    System.out.println("A) " + optionA);
                    System.out.println("C) " + optionC);
                    break;
                case "B":
                    System.out.println("B) " + optionB);
                    System.out.println("C) " + optionC);
                    break;
                case "C":
                    System.out.println("C) " + optionC);
                    System.out.println("D) " + optionD);
                    break;
                case "D":
                    System.out.println("A) " + optionA);
                    System.out.println("D) " + optionD);
                    break;

            }
        } else {
            System.out.println("A) " + optionA);
            System.out.println("B) " + optionB);
            System.out.println("C) " + optionC);
            System.out.println("D) " + optionD);
        }
    }

    public boolean validateAnswer(String input){
        if (input.equals(this.answer)){
            return true;
        }

        return false;
    }

    public void useJokerFiftyFifty() {
        this.isFiftyFiftyUsed = true;
    }

    public void useJokerAskFriend(int questionGroupNumber) {
        if (questionGroupNumber <= 4){
            System.out.println("It is " + this.getAnswer(this.answer));
        } else if (questionGroupNumber <= 7){
            int chance = new Random().nextInt(4);
            if (chance < 3){
                System.out.println("I think it is " + this.getAnswer(this.answer));
            } else {
                String[] asd = {"A", "B", "C", "D"};
                List<String> options = Arrays.asList(asd);
                options.remove(this.answer);
                System.out.println("I think it is " + this.getAnswer(options.get(new Random().nextInt(3))));
            }
        } else if (questionGroupNumber <= 10){
            int chance = new Random().nextInt(2);
            if (chance < 1){
                System.out.println("It should be " + this.getAnswer(this.answer));
            } else {
                String[] asd = {"A", "B", "C", "D"};
                List<String> options = Arrays.asList(asd);
                options.remove(this.answer);
                System.out.println("It should be " + this.getAnswer(options.get(new Random().nextInt(3))));
            }
        } else if (questionGroupNumber <= 14){
            int chance = new Random().nextInt(4);
            if (chance == 3){
                System.out.println("Not really sure, but I think it is " + this.getAnswer(this.answer));
            } else {
                String[] asd = {"A", "B", "C", "D"};
                List<String> options = Arrays.asList(asd);
                String[] availableOptions = options.stream().filter(x -> !x.equals(this.answer)).toArray(String[]::new);
                System.out.println("Not really sure, but I think it is " + this.getAnswer(availableOptions[new Random().nextInt(3)]));
            }
        }
    }

    public void useJokerAskTheAudience(int questionGroupNumber){
        int chance = 100 - (questionGroupNumber * 6);
        int secondChance = chance == 100 ? 0 : new Random().nextInt(100 - chance);
        int thirdChance = 100 - chance - secondChance == 0 ? 0 : new Random().nextInt(100 - chance - secondChance);
        int fourthChance = 100 - chance - secondChance - thirdChance;
        Integer[] chances = { secondChance, thirdChance, fourthChance };
        int chanceToUse = 0;
        String result = "";
        if (this.answer.equals("A")){
            result += "A) " + chance + "% ";
        } else {
            result += "A) " + chances[chanceToUse] + "% ";
            chanceToUse++;
        }

        if (this.answer.equals("B")){
            result += "B) " + chance + "% ";
        } else {
            result += "B) " + chances[chanceToUse] + "% ";
            chanceToUse++;
        }

        if (this.answer.equals("C")){
            result += "C) " + chance + "% ";
        } else {
            result += "C) " + chances[chanceToUse] + "% ";
            chanceToUse++;
        }

        if (this.answer.equals("D")){
            result += "D) " + chance + "% ";
        } else {
            result += "D) " + chances[chanceToUse] + "% ";
        }

        System.out.println(result);
    }

    private String getAnswer(String currentAnswer){
        switch (currentAnswer){
            case "A":
                return this.optionA;
            case "B":
                return this.optionB;
            case "C":
                return this.optionC;
            case "D":
                return this.optionD;
            default:
                return "";
        }
    }
}
