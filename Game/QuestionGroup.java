import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuestionGroup {
    private List<Question> questions;

    private int price;

    private boolean isSaveSum;

    public QuestionGroup(int price, boolean isSaveSum, List<Question> questions) {
        this.questions = questions;
        this.price = price;
        this.isSaveSum = isSaveSum;
    }

    public int getPrice(){
        return this.price;
    }

    public boolean isSaveSum(){
        return this.isSaveSum;
    }

    public Question getQuestion() {
        int rnd = new Random().nextInt(this.questions.size());
        Question currentQuestion = this.questions.get(rnd);
        this.questions = new ArrayList<>();
        this.questions.add(currentQuestion);

        return currentQuestion;
    }
}
