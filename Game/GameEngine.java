import java.util.List;
import java.util.Scanner;

class GameEngine {
    private List<QuestionGroup> questionGroups;

    GameEngine() {
        this.questionGroups = QuestionLoader.loadQuestions();
    }

    void run() {
        Scanner sc = new Scanner(System.in);
        boolean gameIsRunning = true;
        int sum = 0;
        int questionGroupNumber = 0;
        boolean hasUsedFiftyFifty = false;
        boolean hasUsedAudience = false;
        boolean hasUsedFriend = false;
        while(gameIsRunning){
            Question currentQuestion = this.questionGroups.get(questionGroupNumber).getQuestion();
            currentQuestion.print();
            String input = sc.nextLine();
            if (!this.isValid(input)){
                System.out.println(ConsoleColors.RED_BOLD + "Possible options are A, B, C, D, 1 - 50:50 joker, 2 - ask the audience, 3 - ask a friend and 'End' for giving up!" + ConsoleColors.RESET);
            } else {
                if (input.equals("End")){
                    if (questionGroupNumber > 0){
                        questionGroupNumber--;
                        sum = this.questionGroups.get(questionGroupNumber).getPrice();
                    }

                    gameIsRunning = false;
                }
                else if (input.equals("1")) {
                    if (hasUsedFiftyFifty){
                        System.out.println(ConsoleColors.RED_BOLD + "You have already used fifty fifty joker!" + ConsoleColors.RESET);
                    } else {
                        currentQuestion.useJokerFiftyFifty();
                        hasUsedFiftyFifty = true;
                    }
                }
                else if (input.equals("2")) {
                    if (hasUsedAudience){
                        System.out.println(ConsoleColors.RED_BOLD + "You have already used ask the audience joker!" + ConsoleColors.RESET);
                    } else {
                        currentQuestion.useJokerAskTheAudience(questionGroupNumber);
                        hasUsedAudience = true;
                    }
                }
                else if (input.equals("3")) {
                    if (hasUsedFriend){
                        System.out.println(ConsoleColors.RED_BOLD + "You have already used ask friend joker!" + ConsoleColors.RESET);
                    } else {
                        currentQuestion.useJokerAskFriend(questionGroupNumber);
                        hasUsedFriend = true;
                    }
                }
                else if (!currentQuestion.validateAnswer(input)){
                    System.out.println("Sorry, wrong answer!");
                    gameIsRunning = false;
                } else{
                    if (this.questionGroups.get(questionGroupNumber).isSaveSum()){
                        sum = this.questionGroups.get(questionGroupNumber).getPrice();
                    }

                    questionGroupNumber++;
                    if(questionGroupNumber > 14){
                        gameIsRunning = false;
                    }
                }
            }
        }

        System.out.println("You have won a total of " + sum + " leva");
    }

    private boolean isValid(String input){
        switch (input){
            case "A":
            case "B":
            case "C":
            case "D":
            case "1":
            case "2":
            case "3":
            case "End":
                return true;
            default:
                return false;
        }
    }
}
