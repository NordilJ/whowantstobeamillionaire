import java.util.Scanner;

public class WhoWantsToBeAMilionaireNBA {
    public static void main(String[] args) {
        GameEngine engine = new GameEngine();
        Scanner sc = new Scanner(System.in);
        System.out.println(ConsoleColors.RED_BOLD + "Possible options are A, B, C, D, 1 - 50:50 joker, 2 - ask the audience, 3 - ask a friend and 'End' for giving up!" + ConsoleColors.RESET);
        engine.run();
        boolean tryAgain = true;
        while (tryAgain){
            System.out.println("Do you want to try again? Y/N");
            String input = sc.nextLine();
            if (input.equals("Y")){
                engine = new GameEngine();
                engine.run();
            } else if (input.equals("N")){
                tryAgain = false;
            } else {
                System.out.println("Invalid input, please enter Y or N!");
            }
        }

    }
}
